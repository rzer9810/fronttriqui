import React, { useContext, useEffect, useState } from 'react';
import logo from './logo.svg';
import styled from "styled-components";
import './App.css';
import { io } from "socket.io-client";
import socketService from './services/socketService';
import { environment } from './environments';
import { JoinRoom } from './components/joinRoom';
import { GameTable } from './components/gameTable';
import GameContext, { IGameContext } from './contexts/gameContext';

const urlserver = environment.urlServer;

const AppContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 1em;
  background-color: #cdcdcd;
`;

const PrincipalTitle = styled.h1`
  margin: 0;
  color: #2f00dd;
`;

const PrincipalContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

function App() {

  const [isInRoom, setInRoom] = useState(false);
  const [playerSymbol, setPlayerSymbol] = useState<"x" | "o">('x');
  const [isPlayerTurn, setPlayerTurn] = useState(false);
  const [isGameStarted, setGameStarted] = useState(false);

  // Establecer Conexion al servidor Socket
  const connectSocket = async () => {
    const socket = await socketService
      .connect(urlserver)
      .catch((err) => {
        console.log("Error: ", err)
      });
  }

  // Renderizar componentes al cargar app
  useEffect(() => {
    connectSocket()
  }, [])

  const gameContextValue: IGameContext = {
    isInRoom,
    setInRoom,
    playerSymbol,
    setPlayerSymbol,
    isPlayerTurn,
    setPlayerTurn,
    isGameStarted,
    setGameStarted,
  }

  return (
    <GameContext.Provider value={gameContextValue}>
      <AppContainer>
        <PrincipalTitle>Bienvenido al Juego Tic-Tac-Toc</PrincipalTitle>
        <PrincipalContainer>
          {!isInRoom && <JoinRoom />}
          {isInRoom && <GameTable />}
        </PrincipalContainer>
      </AppContainer>
    </GameContext.Provider>
  );
}

export default App;
