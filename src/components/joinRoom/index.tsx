import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import gameContext from "../../contexts/gameContext";
import gameService from "../../services/gameService";
import socketService from "../../services/socketService";

interface IJoinRoom { }

const JoinRoomContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 2em;
`;

const InputRoomId = styled.input`
  height: 30px;
  width: 20em;
  font-size: 17px;
  outline: none;
  border: 1px solid #2f00dd;
  border-radius: 3px;
  padding: 0 10px;
`;

const ButtonJoin = styled.button`
  outline: none;
  background-color: #2f00dd;
  color: #fff;
  font-size: 17px;
  border: 2px solid transparent;
  border-radius: 5px;
  padding: 4px 18px;
  transition: all 230ms ease-in-out;
  margin-top: 1em;
  cursor: pointer;

  &:hover {
    background-color: transparent;
    border: 2px solid #2f00dd;
    color: #2f00dd;
  }
  
`;

const MessageAlert = styled.span`
  color: red;
  margin-top: 1em;
`;

// Unirse a la sala
export function JoinRoom(props: IJoinRoom) {

  const [roomName, setRoomName] = useState("");
  const [isJoining, setJoining] = useState(false);

  const { setInRoom, isInRoom } = useContext(gameContext);

  const handleRoomNameChange = (e: React.ChangeEvent<any>) => {
    const value = e.target.value;
    setRoomName(value);
  }

  // Ingresar a la sala
  const joinRoom = async (e: React.FormEvent) => {
    
    e.preventDefault();

    const socket = socketService.socket;
    if (!roomName || roomName.trim() === "" || !socket) return;

    setJoining(true);

    const joined = await gameService
      .joinGameRoom(socket, roomName)
      .catch((err) => {
        alert(err);
      });

    if (joined) setInRoom(true);

    setJoining(false);
  };

  // Formulario para ingresar ID de la Sala
  return (
    <form onSubmit={joinRoom}>
      <JoinRoomContainer>
        <h4>Ingresa un ID para crear o unirse al Juego</h4>
        <InputRoomId
          placeholder="Id de Sala"
          value={roomName}
          onChange={handleRoomNameChange}
        />
        <MessageAlert>*si quieres crear una partida coloca cualquier codigo y pasaselo a tu compañero</MessageAlert>
        <ButtonJoin type="submit" disabled={isJoining}>{isJoining ? "Ingresando..." : "Ingresar"}</ButtonJoin>
      </JoinRoomContainer>
    </form>
  );
}