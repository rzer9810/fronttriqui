import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import gameContext from "../../contexts/gameContext";
import gameService from "../../services/gameService";
import socketService from "../../services/socketService";
import { IPlayTable, ICellProps } from "../../interfaces";

const GameTableContainer = styled.div`
  display: flex;
  flex-direction: column;
  font-family: "Mochiy Pop P One", cursive;
  position: relative;
`;

const RowContainer = styled.div`
  width: 100%;
  display: flex;
`;

const Cell = styled.div<ICellProps>`
  width: 13em;
  height: 9em;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  cursor: pointer;
  border-top: ${({ borderTop }) => borderTop && "3px solid #2f00dd"};
  border-left: ${({ borderLeft }) => borderLeft && "3px solid #2f00dd"};
  border-bottom: ${({ borderBottom }) => borderBottom && "3px solid #2f00dd"};
  border-right: ${({ borderRight }) => borderRight && "3px solid #2f00dd"};
  transition: all 270ms ease-in-out;
  &:hover {
    background-color: #8d44ad28;
  }
`;

const PlayStopper = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  bottom: 0;
  left: 0;
  z-index: 99;
  cursor: default;
`;

const Xletter = styled.span`
  font-size: 100px;
  color: #8e44ad;
  &::after {
    content: "X";
  }
`;

const Oletter = styled.span`
  font-size: 100px;
  color: #8e44ad;
  &::after {
    content: "O";
  }
`;



export function GameTable() {

  const [matrix, setMatrix] = useState<IPlayTable>([
    [null, null, null],
    [null, null, null],
    [null, null, null],
  ]);

  const {
    playerSymbol,
    setPlayerSymbol,
    setPlayerTurn,
    isPlayerTurn,
    isGameStarted,
    setGameStarted,
  } = useContext(gameContext);

  // definir ganador
  const checkGameState = (matrix: IPlayTable) => {
    for (let i = 0; i < matrix.length; i++) {
      let row = [];
      for (let j = 0; j < matrix[i].length; j++) {
        row.push(matrix[i][j]);
      }

      if (row.every((value) => value && value === playerSymbol)) {
        return [true, false];
      } else if (row.every((value) => value && value !== playerSymbol)) {
        return [false, true];
      }
    }

    for (let i = 0; i < matrix.length; i++) {
      let column = [];
      for (let j = 0; j < matrix[i].length; j++) {
        column.push(matrix[j][i]);
      }

      if (column.every((value) => value && value === playerSymbol)) {
        return [true, false];
      } else if (column.every((value) => value && value !== playerSymbol)) {
        return [false, true];
      }
    }

    if (matrix[1][1]) {
      if (matrix[0][0] === matrix[1][1] && matrix[2][2] === matrix[1][1]) {
        if (matrix[1][1] === playerSymbol) return [true, false];
        else return [false, true];
      }

      if (matrix[2][0] === matrix[1][1] && matrix[0][2] === matrix[1][1]) {
        if (matrix[1][1] === playerSymbol) return [true, false];
        else return [false, true];
      }
    }

    //Compruebe si hay un empate
    if (matrix.every((m) => m.every((v) => v !== null))) {
      return [true, true];
    }

    return [false, false];
  };

  // actualizar el tablero de juego
  const updateGameTable = (
    column: number,
    row: number,
    symbol: "x" | "o") => {

    const newMatrix = [...matrix];

    // cambiar estado de la casilla si se encuentra vacia
    if (newMatrix[row][column] === null || newMatrix[row][column] === "null") {
      newMatrix[row][column] = symbol;
      setMatrix(newMatrix)
    }

    // verificar socket para implementar servicio update
    if (socketService.socket) {
      gameService.updateGame(socketService.socket, newMatrix);
      const [currentPlayerWon, otherPlayerWon] = checkGameState(newMatrix);
      
      if (currentPlayerWon && otherPlayerWon) {
        gameService.gameWin(socketService.socket, "El juego quedo empatado")
        alert("El juego quedo empatado")
      }else if(currentPlayerWon && !otherPlayerWon){
        gameService.gameWin(socketService.socket, "Has Perdido!")
        alert("Has Ganado!")
      }
      setPlayerTurn(false);
    }

  };

  // consumir servicio al actualizar juego
  const handleGameUpdate = () => {
    if (socketService.socket) {
      gameService.onGameUpdate(socketService.socket, (newMatrix) => {
        setMatrix(newMatrix);
        setPlayerTurn(true);
        checkGameState(newMatrix);
      });
    }
  }

  // consumir servicio al iniciar juego
  const handleGameStart = () => {
    if (socketService.socket) {
      gameService.onStartGame(socketService.socket, (options) => {
        setGameStarted(true);
        setPlayerSymbol(options.symbol);

        if (options.start) {
          setPlayerTurn(true);
        } else {
          setPlayerTurn(false);
        }

      });
    }
  }

  //consumir servicio al ganar juego
  const handleGameWin = () => {
    if (socketService.socket) {
      gameService.onGameWin(socketService.socket, (message) => {
        setPlayerTurn(false);
        alert(message);
      })
    }
  }

  useEffect(() => {
    handleGameUpdate();
    handleGameStart();
    handleGameWin();
  }, []);
 
  return (
    <GameTableContainer>
      {!isGameStarted && (
        <h2>Esperando a otro jugador para comenzar el juego!</h2>
      )}
      {(!isGameStarted || !isPlayerTurn) && <PlayStopper />}
      {matrix.map((row, rowIdx) => {
        return (
          <RowContainer>
            {row.map((column, columnIdx) => (
              <Cell
                borderRight={columnIdx < 2}
                borderLeft={columnIdx > 0}
                borderBottom={rowIdx < 2}
                borderTop={rowIdx > 0}
                onClick={() =>
                  updateGameTable(columnIdx, rowIdx, playerSymbol)
                }
              >
                {column && column !== "null" ? (
                  column === "x" ? (
                    <Xletter />
                  ) : (
                    <Oletter />
                  )
                ) : null}
              </Cell>
            ))}
          </RowContainer>
        )
      })}
    </GameTableContainer>
  );

}