export type IPlayTable = Array<Array<string | null>>;

export interface IStartGame {
  start: boolean;
  symbol: "x" | "o";
}

export interface ICellProps {
    borderTop?: boolean;
    borderRight?: boolean;
    borderLeft?: boolean;
    borderBottom?: boolean;
  }