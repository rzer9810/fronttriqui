import { io, Socket } from "socket.io-client";

class SocketService {

    public socket: Socket | null = null;

    // Servicio para conectar el servidor y sockect
    public connect(url: string | any): Promise<Socket> {

        return new Promise((rs, rj) => {

            this.socket = io(url);

            if (!this.socket) {
                return rj();
            }

            this.socket.on("connect", () => {
                rs(this.socket as Socket);
            });

            this.socket.on("connect_error", (err) => {
                console.log("Error de Conexion: ", err);
                rj(err);
            });

        });

    }


}

export default new SocketService();