import { IPlayTable, IStartGame } from "../../interfaces";
import { Socket } from "socket.io-client";

// Servicio para unirse al juego 
class GameService {

  // servicio para unirse a la sala
  public async joinGameRoom(socket: Socket, roomId: string): Promise<boolean | any> {
    return new Promise((rs, rj) => {
      socket.emit("join_game", { roomId });
      socket.on("room_joined", () => rs(true));
      socket.on("room_join_error", ({ error }) => rj(error));
    })
  }

  // servicio para actualizar el juego
  public async updateGame(socket: Socket, gameTable: IPlayTable) {
    socket.emit("update_game", { matrix: gameTable });
  }

  // escuchar servicio updategame
  public async onGameUpdate(
    socket: Socket,
    listener: (matrix: IPlayTable) => void
  ) {
    socket.on("on_game_update", ({ matrix }) => listener(matrix));
  }

  //iniciar servicio para comenzar juego
  public async onStartGame(
    socket: Socket,
    listener: (options: IStartGame) => void
  ) {
    socket.on("start_game", listener);
  }

  // Definir ganador
  public async gameWin(socket: Socket, message: string) {
    socket.emit("game_win", { message });
  }

  // escuchar servicio gameWin
  public async onGameWin(
    socket: Socket,
    listener: (message: string) => void
  ) {
    socket.on("on_game_win", ({ message }) => listener(message));
  }
}

export default new GameService();