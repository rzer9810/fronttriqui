### `npm install`
Instala todas las dependencias antes de iniciar la app

### `npm start`

Ejecuta la aplicación en el modo de desarrollo.\
Abra [http://localhost:3000](http://localhost:3000) para verlo en el navegador.

# librerias usadas
socket.io-client

# Funcionamiento
1. recuerda instalar las dependencias con el comando "npm i"
2. recuerda activar el servidor "servertictactoe" antes de ejecutar la aplicacion
3. Inicia la app con el comando "npm start"
4. al ejecutar la app abre el localhost en otra pestaña o en otro navegador
5. si vas a crear una partida ingresa un codigo cualquiera. (Por ejemplo: "sala1")
6. Despues comparte el codigo pegalo en la otra pestaña del navegador

